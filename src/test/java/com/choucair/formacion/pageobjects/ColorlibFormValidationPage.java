package com.choucair.formacion.pageobjects;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ColorlibFormValidationPage extends PageObject {
	
	// Campo Required
		@FindBy(xpath="//*[@id=\'req\']")
		public WebElementFacade Txtrequired;
			
	// Campo Select Sport
		@FindBy(xpath="//*[@id=\'sport\']")
		public WebElementFacade CmbSport;
		
	// Campo Multiple Select Sport
		@FindBy(xpath="//*[@id=\'sport2\']")
		public WebElementFacade CmbSport2;

	// Campo Url
		@FindBy(xpath="//*[@id=\'url1\']")
		public WebElementFacade TxtUrl;
		
	// Campo Email
		@FindBy(id="email1")
		public WebElementFacade TxtEmail;
		
	// Campo Password
		@FindBy(id="pass1")
		public WebElementFacade TxtPass1;
		
	// Campo Confirmacion Password
		@FindBy(id="pass2")
		public WebElementFacade TxtPass2;
		
	// Campo Tamaño Minimo
		@FindBy(name="minsize1")
		public WebElementFacade TxtMinsize;
		
	// Campo Tamaño Maximo
		@FindBy(name="maxsize1")
		public WebElementFacade TxtMaxsize;
		
	// Campo Number
		@FindBy(id="number2")
		public WebElementFacade TxtNumber;
		
	// Campo IP
		@FindBy(id="ip")
		public WebElementFacade TxtIp;
		
	// Campo Date
		@FindBy(id="date3")
		public WebElementFacade TxtDate;
		
	// Campo Date Earlier
		@FindBy(id="past")
		public WebElementFacade TxtDateEarlier;
		
	// Campo Validate
		@FindBy(xpath="//*[@id='popup-validation']/div[14]/input")
		public WebElementFacade BtnValidate;
		
	// Globo Informativo
		@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
		public WebElementFacade GloboInformativo;
		
		
		public void Required(String DatoPrueba) {
			Txtrequired.click();
			Txtrequired.clear();
			Txtrequired.sendKeys(DatoPrueba);
		}
		
		public void Select_Sport(String DatoPrueba) {
			CmbSport.click();
			CmbSport.selectByVisibleText(DatoPrueba);
		}
		
		public void Multiple_Select(String DatoPrueba) {
			CmbSport2.selectByVisibleText(DatoPrueba);
		}
		
		public void Url(String DatoPrueba) {
			TxtUrl.click();
			TxtUrl.clear();
			TxtUrl.sendKeys(DatoPrueba);
		}
		
		public void Email(String DatoPrueba) {
			TxtEmail.click();
			TxtEmail.clear();
			TxtEmail.sendKeys(DatoPrueba);
		}
		
		public void Password(String DatoPrueba) {
			TxtPass1.click();
			TxtPass1.clear();
			TxtPass1.sendKeys(DatoPrueba);
		}
		
		public void Confirm_Password(String DatoPrueba) {
			TxtPass2.click();
			TxtPass2.clear();
			TxtPass2.sendKeys(DatoPrueba);
		}
		
		public void Minimum_Field(String DatoPrueba) {
			TxtMinsize.click();
			TxtMinsize.clear();
			TxtMinsize.sendKeys(DatoPrueba);
		}
		
		public void Maximum_Field(String DatoPrueba) {
			TxtMaxsize.click();
			TxtMaxsize.clear();
			TxtMaxsize.sendKeys(DatoPrueba);
		}
		
		public void Number(String DatoPrueba) {
			TxtNumber.click();
			TxtNumber.clear();
			TxtNumber.sendKeys(DatoPrueba);
		}
		
		public void Ip(String DatoPrueba) {
			TxtIp.click();
			TxtIp.clear();
			TxtIp.sendKeys(DatoPrueba);
		}
		
		public void Date(String DatoPrueba) {
			TxtDate.click();
			TxtDate.clear();
			TxtDate.sendKeys(DatoPrueba);
		}
		
		public void Date_Earlier(String DatoPrueba) {
			TxtDateEarlier.click();
			TxtDateEarlier.clear();
			TxtDateEarlier.sendKeys(DatoPrueba);
		}
		
		public void Validate( ) {
			BtnValidate.click();
		}
		
		public void Form_Sin_Errores() {
			assertThat(GloboInformativo.isCurrentlyVisible(), is(false));
		}
		
		public void Form_Con_Errores() {
			assertThat(GloboInformativo.isCurrentlyVisible(), is(true));
		}

}
