package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ColorlibFormValidationPage;
import net.thucydides.core.annotations.Step;

public class ColorlibFormValidationSteps {
	
ColorlibFormValidationPage ColorlibFormValidationPage;
	
	@Step
	public void diligenciar_popup_datos_tabla(List<List<String>> Data, int id) {
		ColorlibFormValidationPage.Required(Data.get(id).get(0).trim());
		ColorlibFormValidationPage.Select_Sport(Data.get(id).get(1).trim());
		ColorlibFormValidationPage.Multiple_Select(Data.get(id).get(2).trim());
		ColorlibFormValidationPage.Multiple_Select(Data.get(id).get(3).trim());
		ColorlibFormValidationPage.Url(Data.get(id).get(4).trim());
		ColorlibFormValidationPage.Email(Data.get(id).get(5).trim());
		ColorlibFormValidationPage.Password(Data.get(id).get(6).trim());
		ColorlibFormValidationPage.Confirm_Password(Data.get(id).get(7).trim());
		ColorlibFormValidationPage.Minimum_Field(Data.get(id).get(8).trim());
		ColorlibFormValidationPage.Maximum_Field(Data.get(id).get(9).trim());
		ColorlibFormValidationPage.Number(Data.get(id).get(10).trim());
		ColorlibFormValidationPage.Ip(Data.get(id).get(11).trim());
		ColorlibFormValidationPage.Date(Data.get(id).get(12).trim());
		ColorlibFormValidationPage.Date_Earlier(Data.get(id).get(13).trim());
		ColorlibFormValidationPage.Validate();
	}
	
	@Step
	public void Verificar_Ingreso_Datos_Formulario_Exitoso() {
		ColorlibFormValidationPage.Form_Sin_Errores();
	}
	
	@Step
	public void Verificar_Ingreso_Datos_Formulario_Con_Errores() {
		ColorlibFormValidationPage.Form_Con_Errores();
	}

}
