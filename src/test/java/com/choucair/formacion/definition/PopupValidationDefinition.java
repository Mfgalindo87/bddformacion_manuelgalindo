package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.ColorlibFormValidationSteps;
import com.choucair.formacion.steps.PopupValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {
	
	@Steps
	PopupValidationSteps popupValidationSteps;
	@Steps
	ColorlibFormValidationSteps ColorlibFormValidationSteps;
	
	
	@Given("^Realizar autenticación en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void realizar_autenticación_en_colorlib_con_usuario_y_clave(String Usuario, String Clave) {
		popupValidationSteps.login_colorlib(Usuario, Clave);
	}

	@Given("^Ingresar a la funcionalidad Forms Validation$")
	public void ingresar_a_la_funcionalidad_Forms_Validation() {
		popupValidationSteps.ingresar_form_validation();	    
	}

	@When("^Diligenciar formulario Popup Validation$")
	public void diligenciar_formulario_Popup_Validation(DataTable DtDatosForm) {
		List<List<String>> Data = DtDatosForm.raw();
		
		for (int i=1; i<Data.size(); i++) {
			ColorlibFormValidationSteps.diligenciar_popup_datos_tabla(Data, i);
			try {
				Thread.sleep(5000);
			} catch(InterruptedException e) {}
		}
	}

	@Then("^Validar registro exitoso$")
	public void validar_registro_exitoso() {
		ColorlibFormValidationSteps.Verificar_Ingreso_Datos_Formulario_Exitoso();
	}
	
	@Then("^Verificar presentación de Globo Informativo de validación$")
	public void verificar_presentación_de_Globo_Informativo_de_validación() {
		ColorlibFormValidationSteps.Verificar_Ingreso_Datos_Formulario_Con_Errores();
	}
	    
}
