#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresión
Feature: Formulario Popup Validation
  El usuario debe poder diligenciar los datos requeridos en el formulario.
  Los campos del formulario realizarán validaciones pertinentes de obligatoriedad,
  longitud y formato presentando el resultado correspondiente mediante cuadros
  informativos al momento del diligenciamiento de cada uno de ellos.

  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario Popup Validation,
  no se presenta ningun mensaje de validación.
    Given Realizar autenticación en colorlib con usuario "demo" y clave "demo" 
    And Ingresar a la funcionalidad Forms Validation 
    When Diligenciar formulario Popup Validation
    | Required | Select | MultipleS1 | MultipleS2 | Url | E-mail | Password | ConfPassword | MinField | MaxField | Number | IP | Date | DateEarlier |
    | Si | Football | Tennis | Football | http://www.fifa.com | mfgo@gmail.com | Abc123+ | Abc123+ | 569455 | 125694 | 11.87 | 172.168.1.7 | 2019-08-05 | 2007-08-07 |
    Then Validar registro exitoso 
    
  @CasoAlterno
  Scenario: Diligenciamiento con errores del formulario Popup Validation,
  se presenta globo infomativo indicando error en el diligenciamiento de alguno de los campos.
    Given Realizar autenticación en colorlib con usuario "demo" y clave "demo" 
    And Ingresar a la funcionalidad Forms Validation 
    When Diligenciar formulario Popup Validation
    | Required | Select | MultipleS1 | MultipleS2 | Url | E-mail | Password | ConfPassword | MinField | MaxField | Number | IP | Date | DateEarlier |
    |  | Football | Tennis | Football | http://www.fifa.com | mfgo@gmail.com | Abc123+ | Abc123+ | 569455 | 125694 | 11.87 | 172.168.1.7 | 2019-08-05 | 2007-08-07 |
    | Si | Choose a sport | Tennis | Football | http://www.fifa.com | mfgo@gmail.com | Abc123+ | Abc123+ | 569455 | 125694 | 11.87 | 172.168.1.7 | 2019-08-05 | 2007-08-07 |
    Then Verificar presentación de Globo Informativo de validación

